package com.example;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class DaveController {

   @org.springframework.web.bind.annotation.RequestMapping(value="/dave")
   public String getStuff(){
      return "dave";
   }
   
}

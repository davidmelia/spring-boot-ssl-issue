package com.example;

import org.apache.catalina.core.AprLifecycleListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootSslIssueApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSslIssueApplication.class, args);
	}
	
   @Bean
   public EmbeddedServletContainerCustomizer containerTomcatSpecificCustomizer() {

      return new EmbeddedServletContainerCustomizer() {

         @Override
         public void customize(ConfigurableEmbeddedServletContainer container) {
            /**
             * The next bit is for Tomcat Native (APR). You need to do two things <br/>
             * <ol>
             * <li>sudo apt-get upgrade libtcnative-1</li>
             * <li>add this to the spring Boot start up -Djava.library.path=-Djava.library.path=/usr/local/apr/lib (assuming the library was install here!)</li>
             * </ol>
             */
            if (container instanceof TomcatEmbeddedServletContainerFactory) {
              ((TomcatEmbeddedServletContainerFactory) container).addContextLifecycleListeners(new AprLifecycleListener());
            }

         }
      };
   }
	
}
